<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link rel="stylesheet" href="interface.css">
    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body>
    <?php
    session_start();

    $data = $_SESSION["registry_data"];
    $genderArray = array("1" => "Nam", "2" => "Nữ");
    $faculties = array("0" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");

    ?>

    <div class="my-container border-solid border-2 border-[#2f659c] py-6 px-12">
        <form action="" method="post">

            <div class="flex mb-3">
                <div class="text-center float-left w-1/4">
                    <label class="
                            block
                            p-2
                            border-solid
                            border-2
                            bg-[#1a4e8f]
                            border-[#2f659c]
                            text-white" for="fullName">
                        Họ và tên
                    </label>

                </div>
                <div class="float-left w-3/4">
                    <div class="w-3/4 h-full ml-12 pt-2">
                        <?php echo $data["fullName"] ?>
                    </div>


                </div>
            </div>

            <div class="flex mb-3">
                <div class="text-center float-left w-1/4">
                    <label class="
                            block
                            p-2
                            border-solid
                            border-2
                            bg-[#1a4e8f]
                            border-[#2f659c]
                            text-white">
                        Giới tính
                    </label>

                </div>
                <div class="float-left w-3/4">
                    <div class="w-3/4 h-full ml-12 pt-2">
                        <?php echo $data["gender"] ?>
                    </div>
                </div>
            </div>

            <div class="flex mb-3">
                <div class="text-center float-left w-1/4">
                    <label class="
                            block
                            p-2
                            border-solid
                            border-2
                            bg-[#1a4e8f]
                            border-[#2f659c]
                            text-white" for="faculty">
                        Phân khoa
                    </label>

                </div>
                <div class="float-left w-3/4">
                    <div class="w-3/4 h-full ml-12 pt-2">
                        <?php
                        echo $faculties[$data["faculty"]]
                        ?>
                    </div>
                </div>
            </div>

            <div class="flex mb-3">
                <div class="text-center float-left w-1/4">
                    <label class="
                            block
                            p-2
                            border-solid
                            border-2
                            bg-[#1a4e8f]
                            border-[#2f659c]
                            text-white" for="birthday">
                        Ngày sinh
                    </label>

                </div>
                <div class="float-left w-3/4">
                    <div class="w-3/4 h-full ml-12 pt-2">
                        <?php echo $data["birthday"] ?>
                    </div>
                </div>
            </div>

            <div class="flex mb-3">
                <div class="text-center float-left w-1/4">
                    <label class="
                            block
                            p-2
                            border-solid
                            border-2
                            bg-[#1a4e8f]
                            border-[#2f659c]
                            text-white" for="address">
                        Địa chỉ
                    </label>

                </div>
                <div class="float-left w-3/4">
                    <div class="w-3/4 h-full ml-12 pt-2">
                        <?php echo $data["address"] ?>
                    </div>

                </div>
            </div>

            <div class="flex mb-3">
                <div class="text-center float-left w-1/4">
                    <label class="
                            block
                            p-2
                            border-solid
                            border-2
                            bg-[#1a4e8f]
                            border-[#2f659c]
                            text-white" for="upload-image">
                        Hình ảnh
                    </label>

                </div>
                <div class="float-left w-3/4 mt-2">
                    <div class="w-3/4 h-full ml-12 pt-2">
                        <?php
                        if (isset($data["uploadedImage"])) {
                            $imgURL = $data["uploadedImage"];
                            echo "<img src='$imgURL' class='w-1/2 object-contain' alt='$imgURL'>";
                        }
                        ?>
                    </div>

                </div>
            </div>
            <form method="post" action="./complete_regist.php">
                <div class="flex mt-8">
                    <a href="./complete_regist.php" class="
                            relative
                            left-[37.5%]
                            w-1/4
                            cursor-pointer
                            p-2
                            rounded-lg
                            border-solid
                            border-2
                            bg-[#1a4e8f]
                            border-[#2f659c]
                            text-white
                            text-center">
                        Xác nhận
                    </a>
                </div>
            </form>
    </div>

    <script src="https://unpkg.com/flowbite@1.5.3/dist/datepicker.js"></script>
</body>

</html>