<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Student</title>
    <script src="https://cdn.tailwindcss.com"></script>

    <link rel="stylesheet" href="interface.css">
</head>

<body>
    <?php
    session_start();
    include('./connection.php');
    $name = $_SESSION['registry_data']['fullName'];
    $gender = $_SESSION['registry_data']['gender'];
    $fac  = $_SESSION['registry_data']['faculty'];
    $birthday = date("Y-m-d", strtotime(implode("-", explode("/", $_SESSION['registry_data']['birthday']))));
    $address = $_SESSION['registry_data']['address'];
    if (isset($_SESSION['registry_data']['uploadedImage']))
        $avartar = $_SESSION['registry_data']['uploadedImage'];
    else {
        $avartar = '';
    }
    $sql = "INSERT INTO `student` (`name`, `gender`, `faculty`, `birthday`, `address`, `avartar`) VALUES ('$name', '$gender', '$fac', '$birthday', '$address', '$avartar')";
    $connection->exec($sql);
    ?>

    <div class="my-container mt-1 border-solid border-2 border-[#3599b8]">
        <div class="my-10">
            <div class="">
                <p class="text-center">Bạn đã đăng ký thành công sinh viên</p>
                <p class="text-center">
                    <a class="text-center underline" href="./registed_list.php">Quay lại danh sách sinh viên</a>
                </p>
            </div>
        </div>
    </div>
</body>
</style>

</html>