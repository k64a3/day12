<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Danh sách</title>

    <link rel="stylesheet" href="interface.css">
    <script src="https://cdn.tailwindcss.com"></script>
    <script src="https://unpkg.com/flowbite@1.5.3/dist/datepicker.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</head>

<body>
    <?php
    $departments = array("None" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
    // Get data student từ database 
    include('./connection.php');
    $getData = "SELECT * FROM `student`";
    $getQuantity = "SELECT COUNT(`student`.id) AS COUNT FROM `student`";
    $count = $connection->query($getQuantity);
    $datas = $connection->query($getData);

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $_COOKIE["department"] = $_POST["department"];
        $_COOKIE["keyword"] = $_POST["keyword"];
    }
    ?>

    <div class="my-container py-6 px-12">
        <form action="" method="post" enctype="multipart/form-data">

            <div class="flex mb-3">
                <div class="float-left w-1/4">
                    <label class="
                            block
                            p-2" for="faculty">
                        Khoa
                    </label>

                </div>
                <div class="float-left w-3/4">
                    <div class="h-full">
                        <select class="
                        w-3/4
                        h-full
                        ml-10
                        border-solid
                        border-2
                        border-[#4f85b4]
                        bg-sky-100" name="faculty" id="faculty">
                            <?php
                            $faculty = [
                                '0' => ' ',
                                'MAT' => 'Khoa học máy tính',
                                'KDL' => 'Khoa học vật liệu',
                            ];
                            ?>

                            <?php foreach ($faculty as $key => $value) { ?>
                                <option value="<?= $key ?>" <?= (isset($_POST['faculty']) && $_POST['faculty'] == $key) ? 'selected' : '' ?>><?= $value ?></option>
                            <?php } ?>

                        </select>
                        <div class="arrow inline"></div>
                    </div>
                </div>
            </div>

            <div class="flex mb-3">
                <div class="float-left w-1/4">
                    <label class="
                            block
                            p-2" for="keyword">
                        Từ khóa
                    </label>

                </div>
                <div class="float-left w-3/4">
                    <input class="
                            w-3/4
                            h-full
                            ml-10
                            border-solid
                            border-2
                            border-[#4f85b4]
                            bg-sky-100" type="text" id="keyword" name="keyword" value="<?php echo isset($_POST['keyword']) ? htmlspecialchars($_POST['keyword'], ENT_QUOTES) : ''; ?>">

                </div>
            </div>

            <div class="flex mt-3">
                <div class="w-1/4"></div>
                <div class="w-3/4 float-right">
                    <button name="delete-button" id="delete-button" class="
                            relative
                            left-[23%]
                            w-1/4
                            cursor-pointer
                            p-2
                            rounded-lg
                            border-solid
                            border-2
                            bg-[#1a4e8f]
                            border-[#4f85b4]
                            text-white">
                        Xóa
                    </button>
                    <input class="
                            relative
                            left-[23%]
                            w-1/4
                            cursor-pointer
                            p-2
                            rounded-lg
                            border-solid
                            border-2
                            bg-[#1a4e8f]
                            border-[#4f85b4]
                            text-white" type="submit" value="Tìm kiếm" name="submit">
                </div>

            </div>


        </form>

    </div>

    <div class="wrapper">
        <div class="flex justify-between items-center mx-auto mt-4">
            <div class="students-found">
                <span>Số sinh viên tìm thấy:</span>
                <span>
                    <?php while ($quantity = $count->fetch(PDO::FETCH_ASSOC)) {
                        echo $quantity["COUNT"];
                    } ?>
                </span>
            </div>
            <div class="add-button">
                <form action="index.php">
                    <input type="submit" class="
                            cursor-pointer
                            p-2
                            rounded-lg
                            border-solid
                            border-2
                            bg-[#1a4e8f]
                            border-[#4f85b4]
                            text-white
                            w-[100px]
                            mr-[90px]" value="Thêm">
                </form>
            </div>
        </div>

        <div class="mt-4">
            <table class="w-full">
                <thead>
                    <tr>
                        <th class="text-left">No</th>
                        <th class="text-left">Tên sinh viên</th>
                        <th class="text-left">Khoa</th>
                        <th class="text-left">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $order = 0;
                    while ($data = $datas->fetch(PDO::FETCH_ASSOC)) {
                        $order++;
                        echo '<tr>
                                <td class="text-center">' . $order . '</td>
                                <td>' . $data['name'] . '</td>
                                <td>' . $departments[$data['faculty']] . '</td>
                                <td class="action-table">
                                    <input 
                                        type="submit" 
                                        class="
                                        cursor-pointer 
                                        px-3
                                        py-1 
                                        mt-2
                                        border-solid 
                                        border-2 
                                        bg-[#8BAACF] 
                                        border-[#4f85b4] 
                                        text-white" 
                                    value="Xóa"
                                    >
                                    <input 
                                        type="submit" 
                                        class="
                                            cursor-pointer 
                                            px-3
                                            py-1 
                                            border-solid 
                                            border-2 
                                            bg-[#8BAACF] 
                                            border-[#4f85b4] 
                                            text-white" 
                                        value="Sửa"
                                    >
                                </td>
                            </tr>';
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</body>

<script>
    document.getElementById("delete-button").addEventListener("click", function(event) {
        $('#faculty').val(0);
        $('#keyword').val("");

        event.preventDefault()
    })

    $("#faculty").val(decodeURI(getCookie("faculty")));
    if (typeof getCookie("keyword") !== 'undefined')
        $("#keyword").val(decodeURI(getCookie("keyword")));

    function getCookie(name) {
        const value = `; ${document.cookie}`;
        const parts = value.split(`; ${name}=`);
        if (parts.length === 2) return parts.pop().split(';').shift();
    }
</script>

</html>