<?php
$serverName = "localhost";
$username = "root";
$database = "web_learning";
try {
    $connection = new PDO("mysql:host=$serverName;dbname=$database", $username);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Connection failed" . $e->getMessage();
}
